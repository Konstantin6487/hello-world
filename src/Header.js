import React, {Component} from "react";
import PropTypes from "prop-types";
import {uniqueId} from "lodash";

export default class Header extends Component {
    static propTypes = {
        items: PropTypes.array.isRequired,
        isLoading: PropTypes.bool,
        sumbit: PropTypes.func.isRequired,
        title: PropTypes.string.isRequired,
        type: PropTypes.oneOf(['news', 'photos']),
        user: PropTypes.shape({
            name: PropTypes.string,
            age: PropTypes.number
        }),
        users: PropTypes.arrayOf({
            name: PropTypes.string,
            age: PropTypes.number
        })
    };
    render () {

        const data = this.props.items.map((item) =>
            <a href={item.link} key={uniqueId()}>{item.label}</a>);


        return (
            <div>
                {data}
            </div>
        );

    }

}
