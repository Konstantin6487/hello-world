import React, {Component} from "react";
import "./RegistrationForm.css";

export default class RegistrationForm extends Component {

    constructor (props) {

        super(props);
        this.state = {"email": ""};

    }

    handleSubmit (event) {

        event.preventDefault();
        console.log("form is submitted, Email value is", this.state.email);

    }

    handleEmailChange (event) {

        this.setState({"email": event.target.value});
        console.log("email was changed");


    }

    render () {

        return (
            <form onSubmit={this.handleSubmit.bind(this)}>
                <input
                    type="email"
                    placeholder="email"
                    value={this.state.email}
                    onChange={this.handleEmailChange.bind(this)}
                    className="emailField"
                />
                <button type="submit" className="submitButton">Save</button>
            </form>
        );

    }

}
