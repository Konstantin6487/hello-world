import React, {Component} from "react";

class Dropdown extends Component {

    constructor (props) {

        super(props);
        this.state = {"isOpened": false};

    }

    toggleState () {

        this.setState((prevState) => ({"isOpened": !prevState.isOpened}));

    }

    render () {

        let dropdownText;

        if (this.state.isOpened) {

            dropdownText = <div>Here is what is shown in dropdown</div>;

        }

        return (
            <div onClick={(e) => this.toggleState(e)}>
                Its dropdown, baby
                {dropdownText}
            </div>
        );

    }

}

export default Dropdown;
